package techblue;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;

import uk.co.techblue.accountshome.exception.CustomException;
import uk.co.techblue.accountshome.exception.GatewayServiceException;
import uk.co.techblue.accountshome.service.gateway.XmlRequestGateWay;
import uk.co.techblue.govtalk.request.dto.CompanyDetailsRequest;
import uk.co.techblue.govtalk.request.generic.NameSearchRequest;
import uk.co.techblue.govtalk.responses.dto.CompanyDetailsResponse;
import uk.co.techblue.govtalk.responses.dto.NameSearchResponse;

public class GatewayTest {

	public static void checkCompanyDetails(){
		
		CompanyDetailsRequest companyDetailsRequest=new CompanyDetailsRequest();
		companyDetailsRequest.setCompanyNumber("03176906");
		companyDetailsRequest.setGiveMortTotals(true);
		
		XmlRequestGateWay xmlRequestGateWay=new XmlRequestGateWay();
		try {
			CompanyDetailsResponse companyDetailsResponse=xmlRequestGateWay.getCompanyDetails(companyDetailsRequest, "d.synchronized@gmail.com");
			
			System.out.println(companyDetailsResponse);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CustomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GatewayServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]){
		checkCompanyDetails();
		
		NameSearchRequest nameSearchRequest=new NameSearchRequest();
		nameSearchRequest.setCompanyName("millennium stadium plc");
		nameSearchRequest.setDataSet("LIVE");
		nameSearchRequest.setSameAs(false);
		nameSearchRequest.setSearchRows(new BigInteger("20"));
		
		XmlRequestGateWay xmlRequestGateWay=new XmlRequestGateWay();
		try {
			NameSearchResponse nameSearchResponse=xmlRequestGateWay.getNameSearchResults(nameSearchRequest, "d.synchronized@gmail.com");
			
			System.out.println(nameSearchResponse);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CustomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GatewayServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
