package uk.co.techblue.accountshome.service;

import org.jboss.resteasy.client.ClientResponse;





import uk.co.techblue.accountshome.exception.GatewayServiceException;
import uk.co.techblue.accountshome.resource.AbstractService;
import uk.co.techblue.accountshome.resource.XmlGatewayResource;
import uk.co.techblue.govtalk.dto.GovTalkMessage;
import uk.co.techblue.govtalk.request.generic.GenericGovTalkMessageRequest;
import uk.co.techblue.govtalk.responses.dto.CompanyDetailsResponse;
import uk.co.techblue.govtalk.responses.dto.NameSearchResponse;



public class XmlGatewayService extends AbstractService<XmlGatewayResource>{

	public XmlGatewayService(String restBaseUri) {
		super(restBaseUri);
	}

	@Override
	protected Class<XmlGatewayResource> getResourceClass() {
		return XmlGatewayResource.class;
	}
	
	public CompanyDetailsResponse getCompanyDetailsFromGateway(GenericGovTalkMessageRequest govTalkMessage) throws GatewayServiceException{
		ClientResponse<CompanyDetailsResponse> clientResponse =resourceProxy.getCompanyDetails(govTalkMessage);
		return parseEntityFromResponse(clientResponse, GatewayServiceException.class);		
	}
	
	public NameSearchResponse getNameSearchResultsFromGateway(GenericGovTalkMessageRequest govTalkMessage) throws GatewayServiceException{
		ClientResponse<NameSearchResponse> clientResponse =resourceProxy.getNameSearchResults(govTalkMessage);
		return parseEntityFromResponse(clientResponse, GatewayServiceException.class);		
	}

}
