package uk.co.techblue.accountshome.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;




import org.jboss.resteasy.client.ClientResponse;





import uk.co.techblue.client.Resource;
import uk.co.techblue.govtalk.request.generic.GenericGovTalkMessageRequest;
import uk.co.techblue.govtalk.responses.dto.CompanyDetailsResponse;
import uk.co.techblue.govtalk.responses.dto.NameSearchResponse;



@Path("http://xmlgw.companieshouse.gov.uk/v1-0/xmlgw/Gateway")
public interface XmlGatewayResource extends Resource{

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	ClientResponse<CompanyDetailsResponse> getCompanyDetails(GenericGovTalkMessageRequest govTalkMessage);
	
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	ClientResponse<NameSearchResponse> getNameSearchResults(GenericGovTalkMessageRequest govTalkMessage);
	
	
}
