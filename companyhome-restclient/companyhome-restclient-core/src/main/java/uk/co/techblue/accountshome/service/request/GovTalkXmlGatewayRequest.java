package uk.co.techblue.accountshome.service.request;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import uk.co.techblue.accountshome.exception.CustomException;
import uk.co.techblue.accountshome.service.utility.UtilityConstants;
import uk.co.techblue.govtalk.dto.Authentication;
import uk.co.techblue.govtalk.dto.GovTalkDetails;
import uk.co.techblue.govtalk.dto.Header;
import uk.co.techblue.govtalk.dto.IDAuthentication;
import uk.co.techblue.govtalk.dto.Keys;
import uk.co.techblue.govtalk.dto.MessageDetails;
import uk.co.techblue.govtalk.dto.SenderDetails;
import uk.co.techblue.govtalk.request.generic.GenericGovTalkMessageRequest;
import uk.co.techblue.govtalk.responses.body.Body;

public class GovTalkXmlGatewayRequest {

	private GenericGovTalkMessageRequest govTalkMessage;

	private String transactionId;
	
	private String requestType;

	public GovTalkXmlGatewayRequest() {
		govTalkMessage = new GenericGovTalkMessageRequest();

		generateTransactionId();
	}

	public GenericGovTalkMessageRequest createGovTalkRequest(
			Object requestObject, String emailAddress,String requestType)
			throws NoSuchAlgorithmException, CustomException {

		this.requestType=requestType;
		
		govTalkMessage = new GenericGovTalkMessageRequest();
		// sets the envelope version for the message
		govTalkMessage.setEnvelopeVersion("1.0");
		// sets the govTalk Message Object
		govTalkMessage.setHeader(getHeaderForRequest(emailAddress));
		// sets the gov talk details (Optional)
		govTalkMessage.setGovTalkDetails(getGovTalkDetails());
		// set the message body(object has been kept here to keep the request
		// generic)
		govTalkMessage.setBody(createMessageBody(requestObject));

		return govTalkMessage;

	}

	private Body createMessageBody(Object requestObject) {
		Body body = new Body();
		body.getAny().add(requestObject);
		return body;
	}

	private void generateTransactionId() {

		transactionId = String.valueOf(new Date().getTime()
				+ new Date().getHours() + new Date().getMinutes()
				+ new Date().getYear());
	}

	private Header getHeaderForRequest(final String emailAdressOfSender)
			throws NoSuchAlgorithmException {

		// create a header object which contains the data for the sender and the
		// message details
		Header header = new Header();
		// set the message details
		header.setMessageDetails(getMessageDetails());
		// sets the sender details into the object
		header.setSenderDetails(getSenderDetails(emailAdressOfSender));
		return header;

	}

	private GovTalkDetails getGovTalkDetails() {
		GovTalkDetails govTalkDetails = new GovTalkDetails();
		Keys keys = new Keys();
		govTalkDetails.setKeys(keys);

		return govTalkDetails;
	}

	private MessageDetails getMessageDetails() {

		MessageDetails messageDetails = new MessageDetails();
		// sets the type of the message details class to the message details
		messageDetails.setClazz(requestType);
		// specifies the type of the request
		messageDetails.setQualifier("request");
		// sets the transaction Id
		messageDetails.setTransactionID(transactionId);

		return messageDetails;

	}

	private SenderDetails getSenderDetails(final String emailAdressOfSender)
			throws NoSuchAlgorithmException {

		SenderDetails senderDetails = new SenderDetails();
		IDAuthentication idAuthentication = new IDAuthentication();
		idAuthentication.setSenderID(UtilityConstants.XML_GATEWAY_USERNAME);

		Authentication authentication = new Authentication();
		authentication
				.setMethod(UtilityConstants.GATEWAY_AUTHENTICATION_METHOD);
		authentication.setValue(generateMd5Digest());
		idAuthentication.getAuthentication().add(authentication);

		// set the value into the sender objects
		senderDetails.setEmailAddress(emailAdressOfSender);
		senderDetails.setIDAuthentication(idAuthentication);

		return senderDetails;
	}

	private String generateMd5Digest() throws NoSuchAlgorithmException {
		// concate the username password and transaction id into one string
		final String input = UtilityConstants.XML_GATEWAY_USERNAME
				+ UtilityConstants.XML_GATEWAY_PASSWORD + transactionId;
		// Create MessageDigest object for MD5
		MessageDigest digest = MessageDigest
				.getInstance(UtilityConstants.HASHING_ALGORITHM);
		// Update input string in message digest
		digest.update(input.getBytes(), 0, input.length());
		// Converts message digest value in base 16 (hex)
		return new BigInteger(1, digest.digest()).toString(16);
	}

}
