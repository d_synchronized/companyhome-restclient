package uk.co.techblue.accountshome.service.utility;

public enum RequestType {

	NAME_SEARCH_REQUEST(0),
	
	NUMBER_SEARCH_REQUEST(1),
	
	DOCUMENT_SEARCH_REQUEST(2);
	
	int requestCode;
	
	RequestType(int requestCode){
		this.requestCode=requestCode;
	}
	
	
	
}
