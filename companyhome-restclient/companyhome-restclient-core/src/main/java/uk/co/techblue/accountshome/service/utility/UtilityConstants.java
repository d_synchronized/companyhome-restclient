package uk.co.techblue.accountshome.service.utility;

public class UtilityConstants {

	public static final String GATEWAY_AUTHENTICATION_METHOD = "MD5SIGN";

	public static final String XML_GATEWAY_USERNAME = "XMLGatewayTestUserID";

	public static final String XML_GATEWAY_PASSWORD = "XMLGatewayTestPassword";

	public static final String HASHING_ALGORITHM = "MD5";

	public static final String COMPANY_DETAILS_REQUEST = "CompanyDetails";

	public static final String NAME_SEARCH_REQUEST = "NameSearch";

	public static final String NUMBER_SEARCH_REQUEST = "NumberSearch";

	public static final String DOCUMENT_SEARCH_REQUEST = "DocumentSearch";
	
	public static int COMPANY_DETAILS_REQUEST_CODE=0;
	
	public static int NAME_SEARCH_REQUEST_CODE=1;
	
	public static int DOCUMENT_SEARCH_REQUEST_CODE=2;
	
	public static final String XML_GATEWAY_URI="http://xmlgw.companieshouse.gov.uk/v1-0/xmlgw/Gateway";

}
