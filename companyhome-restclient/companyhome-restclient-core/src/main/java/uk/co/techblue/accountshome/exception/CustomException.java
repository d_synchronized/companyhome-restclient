package uk.co.techblue.accountshome.exception;

public class CustomException extends Exception{

	private static final long serialVersionUID = 3357999958661960760L;

	public CustomException(final String exceptionMessage){
		super(exceptionMessage);
	}
	
}
