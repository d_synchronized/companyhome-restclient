package uk.co.techblue.accountshome.service.gateway;

import java.security.NoSuchAlgorithmException;

import uk.co.techblue.accountshome.exception.CustomException;
import uk.co.techblue.accountshome.exception.GatewayServiceException;
import uk.co.techblue.accountshome.service.XmlGatewayService;
import uk.co.techblue.accountshome.service.request.GovTalkXmlGatewayRequest;
import uk.co.techblue.accountshome.service.utility.RequestType;
import uk.co.techblue.accountshome.service.utility.UtilityConstants;
import uk.co.techblue.govtalk.request.dto.CompanyDetailsRequest;
import uk.co.techblue.govtalk.request.generic.GenericGovTalkMessageRequest;
import uk.co.techblue.govtalk.request.generic.NameSearchRequest;
import uk.co.techblue.govtalk.responses.dto.CompanyDetailsResponse;
import uk.co.techblue.govtalk.responses.dto.NameSearchResponse;

public class XmlRequestGateWay {
	private GovTalkXmlGatewayRequest govTalkXmlGatewayRequest;

	private XmlGatewayService companyDetailsService;

	public XmlRequestGateWay() {
		govTalkXmlGatewayRequest = new GovTalkXmlGatewayRequest();

		companyDetailsService = new XmlGatewayService(UtilityConstants.XML_GATEWAY_URI);
	}

	public CompanyDetailsResponse getCompanyDetails(
			CompanyDetailsRequest companyDetailsRequest, String emailAddress)
			throws NoSuchAlgorithmException, CustomException,
			GatewayServiceException {

		GenericGovTalkMessageRequest genericGovTalkMessageRequest = govTalkXmlGatewayRequest
				.createGovTalkRequest(companyDetailsRequest, emailAddress,UtilityConstants.COMPANY_DETAILS_REQUEST);
		return companyDetailsService
				.getCompanyDetailsFromGateway(genericGovTalkMessageRequest);
	}
	
	public NameSearchResponse getNameSearchResults(
			NameSearchRequest nameSearchRequest, String emailAddress)
			throws NoSuchAlgorithmException, CustomException,
			GatewayServiceException {

		GenericGovTalkMessageRequest genericGovTalkMessageRequest = govTalkXmlGatewayRequest
				.createGovTalkRequest(nameSearchRequest, emailAddress,UtilityConstants.NAME_SEARCH_REQUEST);
		return companyDetailsService
				.getNameSearchResultsFromGateway(genericGovTalkMessageRequest);
	}

	/**
	 * Similar to Company Details Request there will be a call for the name
	 * search and other search request to the gateway
	 * 
	 * */

}
