package uk.co.techblue.namesearch.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PreviousNamesType", propOrder = { "conDate", "companyName" })
public class PreviousNamesType {

	@XmlElement(name = "CONDate", required = true)
	protected XMLGregorianCalendar conDate;
	@XmlElement(name = "CompanyName", required = true)
	protected String companyName;

	/**
	 * Gets the value of the conDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getCONDate() {
		return conDate;
	}

	/**
	 * Sets the value of the conDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setCONDate(XMLGregorianCalendar value) {
		this.conDate = value;
	}

	/**
	 * Gets the value of the companyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the value of the companyName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyName(String value) {
		this.companyName = value;
	}

}
