package uk.co.techblue.namesearch.dto;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "continuationKey", "regressionKey",
		"searchRows", "companySearchItems" })
@XmlRootElement(name = "NameSearch")
public class NameSearch {

	@XmlElement(name = "ContinuationKey", required = true)
	protected String continuationKey;
	@XmlElement(name = "RegressionKey", required = true)
	protected String regressionKey;
	@XmlElement(name = "SearchRows", required = true)
	protected BigInteger searchRows;
	@XmlElement(name = "CoSearchItem")
	protected List<CompanySearchItem> companySearchItems;

	/**
	 * Gets the value of the continuationKey property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getContinuationKey() {
		return continuationKey;
	}

	/**
	 * Sets the value of the continuationKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setContinuationKey(String value) {
		this.continuationKey = value;
	}

	/**
	 * Gets the value of the regressionKey property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRegressionKey() {
		return regressionKey;
	}

	/**
	 * Sets the value of the regressionKey property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setRegressionKey(String value) {
		this.regressionKey = value;
	}

	/**
	 * Gets the value of the searchRows property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getSearchRows() {
		return searchRows;
	}

	/**
	 * Sets the value of the searchRows property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setSearchRows(BigInteger value) {
		this.searchRows = value;
	}

	/**
	 * Gets the value of the coSearchItem property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the coSearchItem property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCoSearchItem().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CompanySearchItem }
	 * 
	 * 
	 */
	public List<CompanySearchItem> getCompanySearchItems() {
		if (companySearchItems == null) {
			companySearchItems = new ArrayList<CompanySearchItem>();
		}
		return this.companySearchItems;
	}

}
