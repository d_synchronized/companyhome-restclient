package uk.co.techblue.govtalk.request.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "companyNumber", "giveMortTotals" })
@XmlRootElement(name = "CompanyDetailsRequest")
public class CompanyDetailsRequest {

	@XmlElement(name = "CompanyNumber", required = true)
	protected String companyNumber;
	@XmlElement(name = "GiveMortTotals")
	protected Boolean giveMortTotals;

	/**
	 * Gets the value of the companyNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyNumber() {
		return companyNumber;
	}

	/**
	 * Sets the value of the companyNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyNumber(String value) {
		this.companyNumber = value;
	}

	/**
	 * Gets the value of the giveMortTotals property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isGiveMortTotals() {
		return giveMortTotals;
	}

	/**
	 * Sets the value of the giveMortTotals property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setGiveMortTotals(Boolean value) {
		this.giveMortTotals = value;
	}

}
