package uk.co.techblue.govtalk.responses.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import uk.co.techblue.govtalk.dto.GovTalkMessage;
import uk.co.techblue.govtalk.request.generic.NameSearchRequest;
import uk.co.techblue.govtalk.responses.body.NameSearchBody;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ NameSearchRequest.class })
@XmlType(name = "", propOrder = { "envelopeVersion", "header",
		"govTalkDetails", "body" })
@XmlRootElement(name = "GovTalkMessage")
public class NameSearchResponse extends GovTalkMessage{

	@XmlElement(name = "Body")
	private NameSearchBody body;

	public NameSearchBody getBody() {
		return body;
	}

	public void setBody(NameSearchBody body) {
		this.body = body;
	}

}
