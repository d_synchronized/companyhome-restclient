package uk.co.techblue.companydetail.dto;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "numGenPartners", "numLimPartners" })
public class LimitedPartnerships {

	@XmlElement(name = "NumGenPartners", required = true)
	protected BigInteger numGenPartners;
	@XmlElement(name = "NumLimPartners", required = true)
	protected BigInteger numLimPartners;

	/**
	 * Gets the value of the numGenPartners property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getNumGenPartners() {
		return numGenPartners;
	}

	/**
	 * Sets the value of the numGenPartners property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setNumGenPartners(BigInteger value) {
		this.numGenPartners = value;
	}

	/**
	 * Gets the value of the numLimPartners property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getNumLimPartners() {
		return numLimPartners;
	}

	/**
	 * Sets the value of the numLimPartners property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setNumLimPartners(BigInteger value) {
		this.numLimPartners = value;
	}

}
