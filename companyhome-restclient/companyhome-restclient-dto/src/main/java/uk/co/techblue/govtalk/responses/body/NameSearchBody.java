package uk.co.techblue.govtalk.responses.body;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;

import uk.co.techblue.companydetail.dto.CompanyDetails;
import uk.co.techblue.namesearch.dto.NameSearch;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "any" })
public class NameSearchBody {
	
	@XmlElement(name="NameSearch",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected NameSearch any;
	@XmlAnyAttribute
	private Map<QName, String> otherAttributes = new HashMap<QName, String>();

	public NameSearch getAny() {
		
		return this.any;
	}

	public Map<QName, String> getOtherAttributes() {
		return otherAttributes;
	}

	public void setAny(NameSearch any) {
		this.any = any;
	}

	public void setOtherAttributes(Map<QName, String> otherAttributes) {
		this.otherAttributes = otherAttributes;
	}


}
