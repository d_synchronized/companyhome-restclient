package uk.co.techblue.govtalk.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@XmlTransient
public class GovTalkMessage {

	@XmlElement(name = "EnvelopeVersion", required = true)
	protected String envelopeVersion;
	@XmlElement(name = "Header", required = true)
	protected Header header;
	@XmlElement(name = "GovTalkDetails", required = true)
	protected GovTalkDetails govTalkDetails;

	/**
	 * Gets the value of the envelopeVersion property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnvelopeVersion() {
		return envelopeVersion;
	}

	/**
	 * Sets the value of the envelopeVersion property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEnvelopeVersion(String value) {
		this.envelopeVersion = value;
	}

	/**
	 * Gets the value of the header property.
	 * 
	 * @return possible object is {@link GovTalkMessage.Header }
	 * 
	 */
	public Header getHeader() {
		return header;
	}

	/**
	 * Sets the value of the header property.
	 * 
	 * @param value
	 *            allowed object is {@link GovTalkMessage.Header }
	 * 
	 */
	public void setHeader(Header value) {
		this.header = value;
	}

	/**
	 * Gets the value of the govTalkDetails property.
	 * 
	 * @return possible object is {@link GovTalkMessage.GovTalkDetails }
	 * 
	 */
	public GovTalkDetails getGovTalkDetails() {
		return govTalkDetails;
	}

	/**
	 * Sets the value of the govTalkDetails property.
	 * 
	 * @param value
	 *            allowed object is {@link GovTalkMessage.GovTalkDetails }
	 * 
	 */
	public void setGovTalkDetails(GovTalkDetails value) {
		this.govTalkDetails = value;
	}

}
