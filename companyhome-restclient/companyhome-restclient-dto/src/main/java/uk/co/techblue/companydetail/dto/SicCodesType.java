package uk.co.techblue.companydetail.dto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SicCodesType", propOrder = {
    "sicText"
})
public class SicCodesType {

    @XmlElement(name = "SicText", required = true)
    protected List<String> sicText;

    /**
     * Gets the value of the sicText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sicText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSicText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSicText() {
        if (sicText == null) {
            sicText = new ArrayList<String>();
        }
        return this.sicText;
    }

}
