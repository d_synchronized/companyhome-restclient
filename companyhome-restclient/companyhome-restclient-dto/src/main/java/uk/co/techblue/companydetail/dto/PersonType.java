package uk.co.techblue.companydetail.dto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonType", propOrder = { "forename", "surname", "title",
		"honours", "dob", "nationality", "personAddress", "personID" })
public class PersonType {

	@XmlElement(name = "Forename")
	protected List<String> forename;
	@XmlElement(name = "Surname", required = true)
	protected String surname;
	@XmlElement(name = "Title", required = true)
	protected String title;
	@XmlElement(name = "Honours", required = true)
	protected String honours;
	@XmlElement(name = "DOB", required = true)
	@XmlSchemaType(name = "date")
	protected XMLGregorianCalendar dob;
	@XmlElement(name = "Nationality", required = true)
	protected String nationality;
	@XmlElement(name = "PersonAddress", required = true)
	protected PersonAddrType personAddress;
	@XmlElement(name = "PersonID")
	protected String personID;

	/**
	 * Gets the value of the forename property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the forename property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getForename().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link String }
	 * 
	 * 
	 */
	public List<String> getForename() {
		if (forename == null) {
			forename = new ArrayList<String>();
		}
		return this.forename;
	}

	/**
	 * Gets the value of the surname property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Sets the value of the surname property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSurname(String value) {
		this.surname = value;
	}

	/**
	 * Gets the value of the title property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTitle(String value) {
		this.title = value;
	}

	/**
	 * Gets the value of the honours property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getHonours() {
		return honours;
	}

	/**
	 * Sets the value of the honours property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setHonours(String value) {
		this.honours = value;
	}

	/**
	 * Gets the value of the dob property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getDOB() {
		return dob;
	}

	/**
	 * Sets the value of the dob property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setDOB(XMLGregorianCalendar value) {
		this.dob = value;
	}

	/**
	 * Gets the value of the nationality property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * Sets the value of the nationality property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNationality(String value) {
		this.nationality = value;
	}

	/**
	 * Gets the value of the personAddress property.
	 * 
	 * @return possible object is {@link PersonAddrType }
	 * 
	 */
	public PersonAddrType getPersonAddress() {
		return personAddress;
	}

	/**
	 * Sets the value of the personAddress property.
	 * 
	 * @param value
	 *            allowed object is {@link PersonAddrType }
	 * 
	 */
	public void setPersonAddress(PersonAddrType value) {
		this.personAddress = value;
	}

	/**
	 * Gets the value of the personID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPersonID() {
		return personID;
	}

	/**
	 * Sets the value of the personID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPersonID(String value) {
		this.personID = value;
	}

}
