package uk.co.techblue.companydetail.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompanySearchItem", propOrder = { "companyName",
		"companyNumber", "dataSet", "companyIndexStatus", "companyDate",
		"searchMatch" })
public class CoSearchCPLXType {

	@XmlElement(name = "CompanyName", required = true)
	protected String companyName;
	@XmlElement(name = "CompanyNumber", required = true)
	protected String companyNumber;
	@XmlElement(name = "DataSet", required = true)
	protected String dataSet;
	@XmlElement(name = "CompanyIndexStatus")
	protected String companyIndexStatus;
	@XmlElement(name = "CompanyDate")
	@XmlSchemaType(name = "date")
	protected XMLGregorianCalendar companyDate;
	@XmlElement(name = "SearchMatch")
	protected String searchMatch;

	/**
	 * Gets the value of the companyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the value of the companyName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyName(String value) {
		this.companyName = value;
	}

	/**
	 * Gets the value of the companyNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyNumber() {
		return companyNumber;
	}

	/**
	 * Sets the value of the companyNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyNumber(String value) {
		this.companyNumber = value;
	}

	/**
	 * Gets the value of the dataSet property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDataSet() {
		return dataSet;
	}

	/**
	 * Sets the value of the dataSet property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDataSet(String value) {
		this.dataSet = value;
	}

	/**
	 * Gets the value of the companyIndexStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyIndexStatus() {
		return companyIndexStatus;
	}

	/**
	 * Sets the value of the companyIndexStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyIndexStatus(String value) {
		this.companyIndexStatus = value;
	}

	/**
	 * Gets the value of the companyDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getCompanyDate() {
		return companyDate;
	}

	/**
	 * Sets the value of the companyDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setCompanyDate(XMLGregorianCalendar value) {
		this.companyDate = value;
	}

	/**
	 * Gets the value of the searchMatch property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSearchMatch() {
		return searchMatch;
	}

	/**
	 * Sets the value of the searchMatch property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSearchMatch(String value) {
		this.searchMatch = value;
	}

}
